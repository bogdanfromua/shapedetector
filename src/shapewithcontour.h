#ifndef SHAPEWITHCONTOURS_H
#define SHAPEWITHCONTOURS_H


#include "shape.h"
#include "shapecontour.h"


class ShapeWithContour : public Shape
{
private: // fields
    ShapeContour contour;

public: // methods
    ShapeWithContour(const ShapeContour &contour, double inaccuracyPercent);
    ShapeWithContour(const ShapeContour &contour, const ShapeProperties &properties);
    ShapeWithContour(const ShapeContour &contour, double inaccuracyPercent, const std::map<ShapeType, double> &types);
    ShapeWithContour(const ShapeContour &contour, const ShapeProperties &properties, const std::map<ShapeType, double> &types);
    ShapeContour getContour() const;
    void setContour(const ShapeContour &contour);
};


#endif // SHAPEWITHCONTOURS_H
