#ifndef SHAPE_H
#define SHAPE_H


#include <map>
#include "shapeproperties.h"


typedef std::string ShapeType;
#define UNKNOWN     "unknown"


class Shape
{
protected: // fields
    ShapeProperties properties;
    std::map<ShapeType, double> types;

public: // methods
    Shape(const ShapeProperties &properties);
    Shape(const ShapeProperties &properties, const std::map<ShapeType, double> &types);
    Shape(const ShapeProperties &properties, const ShapeType &type);
    std::map<ShapeType, double> getTypes() const;
    void setTypes(std::map<ShapeType, double> types);
    ShapeProperties getProperties() const;
    void setProperties(ShapeProperties properties);
    std::pair<ShapeType, double> getBestType() const;
};


#endif // SHAPE_H
