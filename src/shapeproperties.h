#ifndef SHAPEPROPERTYES_H
#define SHAPEPROPERTYES_H


#include <map>
#include "shapeproperty.h"
#include "shapecontour.h"


#define DEFAULT_INACCURACY_PERCENT 0.15


class ShapeProperties
{
private: // fields
    int cornersCount;
    bool cornersCountEnable;
    std::map<std::pair<int, int>, ShapeProperty> angles;
    std::map<std::pair<int, int>, ShapeProperty> sidesRatios;
    ShapeProperty moment;               // TODO2: rename to ellipseProportion

private: // methods
    int nextSide(int side);
    int nextPoint(int point);
    int prevPoint(int point);
    int sideFromPoints(int pointA, int pointB);
    std::pair<int, int> cornerFromPoint(int point);
    std::pair<int, int> getPoints(int side);
    double getAangle(const ShapeContour &shapeContour, int point);
    static double distance(cv::Point point1, cv::Point point2);

public: // methods
    ShapeProperties(const ShapeContour &shapeContour, double inaccuracyPercent = DEFAULT_INACCURACY_PERCENT);
    ShapeProperties(int cornersCount);
    int getCornersCount() const;
    std::map<std::pair<int, int>, ShapeProperty> getAngles() const;
    std::map<std::pair<int, int>, ShapeProperty> getSidesRatios() const;
    ShapeProperty getMoment() const;
    void setCornersCount(int cornersCount);
    void setAngles(const std::map<std::pair<int, int>, ShapeProperty> &angles);
    void setSidesRatios(const std::map<std::pair<int, int>, ShapeProperty> &sidesRatios);
    void setMoment(const ShapeProperty &moment);
    void setCornersCountEnable(bool enable);
    bool getCornersCountEnable() const;
    double isSubsetOfWithoutRotation(const ShapeProperties &superSetShape) const;
    double isSubsetOf(const ShapeProperties &superSetShape) const;
    void rotate(int steps);
    void removeSameSides();

    cv::RotatedRect minElipse; // TODO1: just for tests
};


#endif // SHAPEPROPERTYES_H
