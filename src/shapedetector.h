#ifndef SHAPEDETECTOR_H
#define SHAPEDETECTOR_H


#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <list>
#include <map>
#include "shape.h"
#include "shapewithcontour.h"
#include "shapecontour.h"
#include <QJsonArray>
#include <QJsonObject>


typedef cv::Mat Image;


typedef int DetectionMethod;
#define CANNY_EXTERNAL          1
#define THRESHOLD_EXTERNAL      2
#define CANNY_TREE              3
#define THRESHOLD_TREE          4





class ShapeDetector
{
private: // fields
    std::vector<Shape> etalonShapes;
    double aproximationAcuracy;
    double detectionCoef1;
    double detectionCoef2;
    DetectionMethod detectionMethod;
    double inaccuracyPercent;
    int erosion_size;

private: // methods
    Image filterImage(const Image& image) const;
    Image filterImageToEdges(const Image& image) const;
    std::vector<ShapeContour> findContours(const Image& image) const;
    std::vector<ShapeContour> approximateContours(const std::vector<ShapeContour> &shapeContours) const;
    std::vector<ShapeWithContour> getShapesFromContours(const std::vector<ShapeContour> &shapeContours) const;
    static std::map<ShapeType, double> classifyShapeByEtalonShapes(const Shape &shape, const std::vector<Shape> &etalonShapes);
    static ShapeProperty jsonObjectToShapeProperty(const QJsonObject &jsonObject);
    static std::map<std::pair<int, int>, ShapeProperty> jsonArrayToSideAndSideAndShapeProperty(const QJsonArray &jsonArray);
    static Shape jsonObjectToShape(const QJsonObject &jsonObject);
    static std::map<ShapeType, double> jsonArrayToShapeTypes(const QJsonArray &jsonArray);
    static QJsonObject shapePropertyToJsonObject(const ShapeProperty & shapeProperty);
    static QJsonArray typesToJsonArray(const std::map<ShapeType, double> & types);
    static QJsonArray sideAndSideAndShapePropertyToJsonArray(const std::map<std::pair<int, int>, ShapeProperty> &sidesAndShapeProperties);
    static QJsonObject shapeToJsonObject(const ShapeWithContour &shape, bool withContours = true, bool withAnglesAndSidesRatios = true);
    static QJsonArray shapesToJsonArray(const std::vector<ShapeWithContour> &shapes, bool withContours = true, bool withAnglesAndSidesRatios = true);

public: // static methods
    ShapeDetector(double aproximationAcuracy = 0.01);
    static Image readImage(const std::string& imagePath);
    static void outImage(const Image& image, const std::string& name);
    static Image markShapesOnImage(const Image& srcImage, const std::vector<ShapeWithContour>& shapes);
    static QByteArray saveShapesToJson(const std::vector<ShapeWithContour> &shapes, bool withContours = true, bool withAnglesAndSidesRatios = true);
    static std::vector<Shape> loadShapesFromJson(const QByteArray & shapesJsonContent);

public: // methods
    std::vector<Shape> getEtalonShapes() const;
    void setEtalonShapes(const std::vector<Shape> &shapes);
    std::vector<ShapeWithContour> getShapesFromImage(const Image& srcImage) const;
    void classifyShape(Shape& shapes) const;
    void setParameters(double aproximationAcuracy = 0.01, double c1 = 0, double c2 = 50, DetectionMethod method = CANNY_EXTERNAL, double inaccuracyPercent = 0.15, int erosion_size = 0);
    double getAproximationAcuracy();
};


#endif // SHAPEDETECTOR_H
