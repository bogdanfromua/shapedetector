#include "shapeproperties.h"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;


int ShapeProperties::nextSide(int side)
{
    if (side == 0) {
        return 0;
    } else if (side == cornersCount) {
        return 1;
    } else {
        return side + 1;
    }
}

int ShapeProperties::nextPoint(int point)
{
    if (point == cornersCount - 1) {
        return 0;
    } else {
        return point + 1;
    }
}

int ShapeProperties::prevPoint(int point)
{
    if (point == 0) {
        return cornersCount - 1;
    } else {
        return point - 1 ;
    }
}

int ShapeProperties::sideFromPoints(int pointA, int pointB)
{
    if (pointA == pointB) {
        throw exception();
    }
    //int first = pointA < pointB ? pointA : pointB;
    int second = pointA < pointB ? pointB : pointA;

    if (second == 0) {
        return cornersCount;
    } else {
        return second;
    }
}

pair<int, int> ShapeProperties::cornerFromPoint(int point)
{
    int side1 = sideFromPoints(prevPoint(point), point);
    int side2 = sideFromPoints(point, nextPoint(point));
    return pair<int, int>(side1, side2);
}

double ShapeProperties::distance(cv::Point point1, cv::Point point2)
{
    double x = point1.x - point2.x;
    double y = point1.y - point2.y;
    return sqrt(x*x + y*y);
}









static double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
/*
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
*/

    double pt1pt0 = sqrt(pow(pt0.x-pt1.x,2) + pow(pt0.y-pt1.y,2));
    double pt2pt0 = sqrt(pow(pt0.x-pt2.x,2) + pow(pt0.y-pt2.y,2));
    double pt1pt2 = sqrt(pow(pt1.x-pt2.x,2) + pow(pt1.y-pt2.y,2));

    double rads = acos(( pow(pt1pt0,2)+ pow(pt2pt0,2) - pow(pt1pt2,2) ) / ( 2 * pt1pt0 * pt2pt0 ));
    double grads = rads * 180 / 3.14;
    return grads;
}

cv::Point getMidlePoint(cv::Point pt1, cv::Point pt2)
{
    return cv::Point((pt1.x + pt2.x)/2, (pt1.y + pt2.y)/2);
}

bool isPointInsideShape(cv::Point pt, const ShapeContour &shapeContour)
{
    return cv::pointPolygonTest(shapeContour, pt, false);
}








double ShapeProperties::getAangle(const ShapeContour &shapeContour, int pointAngle)
{
    int point1 = prevPoint(pointAngle);
    int point2 = nextPoint(pointAngle);
    double angleGrad = angle(shapeContour[point1], shapeContour[point2], shapeContour[pointAngle]);
    cv::Point midlePoint = getMidlePoint(shapeContour[point1], shapeContour[point2]);
    bool isMidlePointInsideShape = isPointInsideShape(midlePoint, shapeContour);
    angleGrad = isMidlePointInsideShape ? angleGrad : 360 - angleGrad;
    return angleGrad;
}

pair<int, int> ShapeProperties::getPoints(int side)
{
    int point1 = side - 1;
    int point2 = nextPoint(point1);
    return pair<int, int>(point1, point2);
}

ShapeProperties::ShapeProperties(const ShapeContour &shapeContour, double inaccuracyPercent)
{
    // geometry properties
    double p = cv::arcLength(shapeContour, true);
    double s = cv::contourArea(shapeContour);

    // moment
    if (shapeContour.size() >= 5) {
        cv::RotatedRect minElipse = cv::fitEllipse( cv::Mat(shapeContour) );
        this->minElipse = minElipse;    // TODO1: just for tests
        double minEllipseR1 = minElipse.size.height / 2;
        double minEllipseR2 = minElipse.size.width / 2;
        double minEllipseProportion = minEllipseR1 < minEllipseR2 ? minEllipseR1/minEllipseR2 : minEllipseR2/minEllipseR1;
        double minEllipseS = 3.14 * minEllipseR1 * minEllipseR2;
        if (s + s*inaccuracyPercent/2 > minEllipseS) {
            moment.enable = true;
            moment.value = minEllipseProportion;
            moment.inaccuracy = moment.value * inaccuracyPercent * 1.5;
        } else {
            moment.enable = false;
        }
    } else {
        moment.enable = false;
    }

    // corners cnt
    cornersCount = shapeContour.size();

    // cornersCountEnable
    if ( moment.enable == true && shapeContour.size() >= 10 ) {     // TODO2: use sides acuracy validation for set cornersCountEnable = false only when sides acuracy is realy bad
        cornersCountEnable = false;
    } else {
        cornersCountEnable = true;
    }

    for (int side1 = 1; side1 <= cornersCount; side1++) {
        // sidesRatios
        pair<int, int> point1 = getPoints(side1);
        double side1Length = distance(shapeContour[point1.first], shapeContour[point1.second]);
        double value = side1Length/p;
        double inacuracy = value*inaccuracyPercent;
        if (  value > 4 || value < 0.25 ) {
            inacuracy = inacuracy * 1.5;
        }
        if ( value > 8 || value < 0.125 ) {
            inacuracy = inacuracy * 1.5;
        }
        this->sidesRatios[pair<int, int>(side1, 0)] = ShapeProperty(value, inacuracy, true);
        for (int side2 = 1; side2 <= cornersCount; side2++) {
            if ( (side1 == side2) /*|| (this->sidesRatios.count(pair<int, int>(side2, side1)) != 0)*/ ) {
                continue;
            }
            pair<int, int> point2 = getPoints(side2);
            double side2Length = distance(shapeContour[point2.first], shapeContour[point2.second]);
            double value = side1Length/side2Length;
            double inacuracy = value*inaccuracyPercent;
            if (  value > 4 || value < 0.25 ) {
                inacuracy = inacuracy * 1.5;
            }
            if ( value > 8 || value < 0.125 ) {
                inacuracy = inacuracy * 1.5;
            }
            this->sidesRatios[pair<int, int>(side1, side2)] = ShapeProperty(value, inacuracy, true);
        }
        // angles
        this->angles[pair<int, int>(side1, nextSide(side1))] = ShapeProperty(getAangle(shapeContour, point1.second), 90*inaccuracyPercent, true);
    }
}

ShapeProperties::ShapeProperties(int cornersCount)
    :cornersCount(cornersCount)
{
    moment.enable = false;
    cornersCountEnable = true;
}

double ShapeProperties::isSubsetOfWithoutRotation(const ShapeProperties &superSetShape) const
{
    double chance = 1;

    // corners
    if ( superSetShape.cornersCountEnable && this->cornersCount != superSetShape.getCornersCount() ) {
        return 0;
    }

    // moment
    if ( superSetShape.moment.enable ) {
        if ( !this->moment.enable ) {
            return 0;
        }
        chance = chance * this->moment.isSubsetOf(superSetShape.moment);
    }

    // angles
    map<pair<int, int>, ShapeProperty>::const_iterator angleIt;
    for (angleIt = superSetShape.angles.begin(); angleIt != superSetShape.angles.end(); angleIt++) {
        if ( !angleIt->second.enable ) {
            continue;
        }

        int side1 = angleIt->first.first;
        int side2 = angleIt->first.second;

        if ( this->angles.count(pair<int, int>(side1, side2)) == 0 ) {
            return 0;
        }

        const ShapeProperty &super = angleIt->second;
        const ShapeProperty &test = this->angles.find(pair<int, int>(side1, side2))->second;

        if ( !test.enable ) {
            return 0;
        }

        chance = chance * test.isSubsetOf(super);
    }

    // sidesRatios
    map<pair<int, int>, ShapeProperty>::const_iterator sidesRatioIt;
    for (sidesRatioIt = superSetShape.sidesRatios.begin(); sidesRatioIt != superSetShape.sidesRatios.end(); sidesRatioIt++) {
        if ( !sidesRatioIt->second.enable ) {
            continue;
        }

        int side1 = sidesRatioIt->first.first;
        int side2 = sidesRatioIt->first.second;

        if ( this->sidesRatios.count(pair<int, int>(side1, side2)) == 0 ) {
            return 0;
        }

        const ShapeProperty &super = sidesRatioIt->second;
        const ShapeProperty &test = this->sidesRatios.find(pair<int, int>(side1, side2))->second;

        if ( !test.enable ) {
            return 0;
        }

        chance = chance * test.isSubsetOf(super);
    }

    return chance;
}

void ShapeProperties::rotate(int steps)
{
    for (int i = 0; i < steps; i++) {
        // angles
        map<pair<int, int>, ShapeProperty> newAngles;
        map<pair<int, int>, ShapeProperty>::iterator angleIt;
        for (angleIt = this->angles.begin(); angleIt != this->angles.end(); angleIt++ ) {
            int newSide1 = nextSide(angleIt->first.first);
            int newSide2 = nextSide(angleIt->first.second);
            newAngles[pair<int, int>(newSide1, newSide2)] = angleIt->second;
        }

        // sidesRatios
        map<pair<int, int>, ShapeProperty> newSidesRatios;
        map<pair<int, int>, ShapeProperty>::iterator sidesRatioIt;
        for (sidesRatioIt = this->sidesRatios.begin(); sidesRatioIt != this->sidesRatios.end(); sidesRatioIt++ ) {
            int newSide1 = nextSide(sidesRatioIt->first.first);
            int newSide2 = nextSide(sidesRatioIt->first.second);
            newSidesRatios[pair<int, int>(newSide1, newSide2)] = sidesRatioIt->second;
        }

        this->angles = newAngles;
        this->sidesRatios = newSidesRatios;
    }
}

double ShapeProperties::isSubsetOf(const ShapeProperties &superSetShape) const
{
    /*
    // check corners                                            // TODO2: do it only in isSubsetOfWithoutRotation
    if ( this->cornersCount != superSetShape.getCornersCount() ) {
        return 0;
    }*/

    double bestChance = 0;
    ShapeProperties testShapeProperties = *this;

    for (int i = 0; i < cornersCount; i++) {
        double chance = testShapeProperties.isSubsetOfWithoutRotation(superSetShape);
        if (bestChance < chance) {
            bestChance = chance;
        }
        testShapeProperties.rotate(1);
    }

    return bestChance;
}

void ShapeProperties::removeSameSides()
{
    std::map<std::pair<int, int>, ShapeProperty>::iterator sidesRatiosIt = sidesRatios.begin();
    while ( sidesRatiosIt != sidesRatios.end() ) {
        if ( this->sidesRatios.count(pair<int, int>(sidesRatiosIt->first.second, sidesRatiosIt->first.first)) != 0 ) {
            this->sidesRatios.erase(pair<int, int>(sidesRatiosIt->first.second, sidesRatiosIt->first.first));
            sidesRatiosIt = sidesRatios.begin();
        } else {
            sidesRatiosIt++;
        }
    }
}

int ShapeProperties::getCornersCount() const
{
    return cornersCount;
}

std::map<std::pair<int, int>, ShapeProperty> ShapeProperties::getAngles() const
{
    return angles;
}

std::map<std::pair<int, int>, ShapeProperty> ShapeProperties::getSidesRatios() const
{
    return sidesRatios;
}

ShapeProperty ShapeProperties::getMoment() const
{
    return moment;
}

void ShapeProperties::setCornersCount(int cornersCount)
{
    this->cornersCount = cornersCount;
}

void ShapeProperties::setAngles(const std::map<std::pair<int, int>, ShapeProperty> &angles)
{
    this->angles = angles;
}

void ShapeProperties::setSidesRatios(const std::map<std::pair<int, int>, ShapeProperty> &sidesRatios)
{
    this->sidesRatios = sidesRatios;
}

void ShapeProperties::setMoment(const ShapeProperty &moment)
{
    this->moment = moment;
}

void ShapeProperties::setCornersCountEnable(bool enable)
{
    this->cornersCountEnable = enable;
}

bool ShapeProperties::getCornersCountEnable() const
{
    return cornersCountEnable;
}
