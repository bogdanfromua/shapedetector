#include "shapeproperty.h"


double ShapeProperty::isSubsetOf(const ShapeProperty &superSetShapeProperty) const {
    // TODO2: use Gaussian distribution
    if (
            superSetShapeProperty.value - superSetShapeProperty.inaccuracy <= value &&
            value <= superSetShapeProperty.value
    ) {
        double a = (superSetShapeProperty.value - value);
        double b = (value - (superSetShapeProperty.value - superSetShapeProperty.inaccuracy));
        return b / (a + b);
    }
    else if (
            superSetShapeProperty.value <= value &&
            value <= superSetShapeProperty.value + superSetShapeProperty.inaccuracy
    ) {
        double a = (value - superSetShapeProperty.value);
        double b = (superSetShapeProperty.value + superSetShapeProperty.inaccuracy - value);
        return b / (a + b);
    }
    else {
        return 0;
    }
}
