#include "shapedetector.h"


#include <cmath>
#include <iostream>
#include <string>
#include "/usr/local/include/opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdexcept>
#include <boost/math/special_functions/round.hpp>
#include <QJsonDocument>
#include <QByteArray>
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>


using namespace std;


ShapeDetector::ShapeDetector(double aproximationAcuracy)
    :aproximationAcuracy(aproximationAcuracy) {}

Image ShapeDetector::readImage(const string& imagePath)
{
    cv::Mat image = cv::imread(imagePath.c_str());
    if (image.empty())
        throw runtime_error("Cant load image");

    return image;
}

Image ShapeDetector::markShapesOnImage(const Image & srcImage, const vector<ShapeWithContour> & shapes)
{
    cv::Mat resultImage = srcImage.clone();

    for (unsigned int i = 0; i < shapes.size(); i++) {
         // TODO1: make it better

        int fontface = cv::FONT_HERSHEY_SIMPLEX;
        double scale = 0.4;
        int thickness = 1;
        int baseline = 0;

        cv::Size text = cv::getTextSize(shapes[i].getBestType().first, fontface, scale, thickness, &baseline);  // TODO2: check for empty types
        cv::Rect r = cv::boundingRect(shapes[i].getContour());

        cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
        cv::rectangle(resultImage, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255,255,255), CV_FILLED);
        cv::putText(resultImage, shapes[i].getBestType().first, pt, fontface, scale, CV_RGB(0,0,0), thickness, 8);

        // contour
        vector<ShapeContour> shapeContours;
        shapeContours.push_back(shapes[i].getContour());
        cv::drawContours( resultImage, shapeContours, 0, CV_RGB(256,0,0), 1, 8, vector<cv::Vec4i>(), 0, cv::Point() );

        ellipse( resultImage, shapes[i].getProperties().minElipse, CV_RGB(0,0,256), 1, 8 );     // TODO1: just for tests
    }

    return resultImage;
}

void ShapeDetector::outImage(const Image & image, const string & name)
{
    cv::imshow(name.c_str(), image);
}

Image ShapeDetector::filterImage(const Image & image) const
{
    cv::Mat result;
    cv::Mat tp1;

    // grayscale
    cv::cvtColor(image, tp1, CV_BGR2GRAY);

    // erosion
    if (erosion_size != 0) {
        cv::Mat element = cv::getStructuringElement(
                    cv::MORPH_CROSS,
                    cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                    cv::Point( erosion_size, erosion_size ) );
        cv::erode(tp1, result, element);
    } else {
        result = tp1;
    }


    // TODO2: check for valid result

    return result;
}

Image ShapeDetector::filterImageToEdges(const Image & image) const
{
    cv::Mat result;

    if ( detectionMethod == CANNY_EXTERNAL || detectionMethod == CANNY_TREE ) {
        cv::Canny( image, result, detectionCoef1, detectionCoef2, 5 );
    } else if ( detectionMethod == THRESHOLD_EXTERNAL || detectionMethod == THRESHOLD_TREE ) {
        cv::threshold( image, result, detectionCoef1, detectionCoef2, cv::THRESH_BINARY );
    }

    // TODO2: check for valid result

    return result;
}

vector<ShapeContour> ShapeDetector::findContours(const Image & image) const
{
    vector<ShapeContour> result;

    if ( detectionMethod == CANNY_EXTERNAL || detectionMethod == THRESHOLD_EXTERNAL ) {
        cv::findContours( image.clone(), result, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );
    } else if ( detectionMethod == CANNY_TREE || detectionMethod == THRESHOLD_TREE )  {
        vector<cv::Vec4i> hierarchy;
        cv::findContours( image.clone(), result, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
        result.erase(result.begin());
    }

    return result;
}

vector<ShapeContour> ShapeDetector::approximateContours(const vector<ShapeContour> & shapeContours) const
{
    vector<ShapeContour> result;

    vector<ShapeContour>::const_iterator it;
    for (it = shapeContours.begin(); it != shapeContours.end(); it++) {
        ShapeContour approx;

        // Approximate contour with accuracy proportional to the contour perimeter
        cv::approxPolyDP(cv::Mat(*it), approx, cv::arcLength(cv::Mat(*it), true)*aproximationAcuracy, true);

        // Skip small objects                                     // TODO2: move to separate method "filerContours"
        if (fabs(cv::contourArea(*it)) < 100) {
            continue;
        }

        result.push_back(approx);
    }
    // TODO2: check for valid result

    return result;
}

vector<ShapeWithContour> ShapeDetector::getShapesFromContours(const vector<ShapeContour> & shapeContours) const
{
    vector<ShapeWithContour> shapes;

    vector<ShapeContour>::const_iterator it;
    for (it = shapeContours.begin(); it != shapeContours.end(); it++) {
        ShapeWithContour shape(*it, inaccuracyPercent);
        shapes.push_back(shape);
    }
    // TODO2: check for valid result

    return shapes;
}

vector<ShapeWithContour> ShapeDetector::getShapesFromImage(const Image & srcImage) const
{
    cv::Mat filtered = filterImage(srcImage);
    ShapeDetector::outImage(filtered, "filtered");              // TODO1: it is just for tests here
    cv::Mat edges = filterImageToEdges(filtered);
    ShapeDetector::outImage(edges, "edges");                    // TODO1: it is just for tests here
    vector<ShapeContour> contours = findContours(edges);
    vector<ShapeContour> approximatedContours = approximateContours(contours);
    vector<ShapeWithContour> shapes = getShapesFromContours(approximatedContours);
    return shapes;
}

map<ShapeType, double> ShapeDetector::classifyShapeByEtalonShapes(const Shape & shape, const vector<Shape> & etalonShapes)
{
    map<ShapeType, double> types;

    vector<Shape>::const_iterator etalonIt;
    for (etalonIt = etalonShapes.begin(); etalonIt != etalonShapes.end(); etalonIt++) {
        if(etalonIt->getTypes().empty()) {
            continue;
        }
        types[etalonIt->getTypes().begin()->first] = shape.getProperties().isSubsetOf(etalonIt->getProperties()) * etalonIt->getTypes().begin()->second;	// TODO2: use all types, not only first
    }

    return types;
}

void ShapeDetector::classifyShape(Shape & shape) const
{
    shape.setTypes(classifyShapeByEtalonShapes(shape, etalonShapes));
}

vector<Shape> ShapeDetector::getEtalonShapes() const
{
    return this->etalonShapes;
}

void ShapeDetector::setEtalonShapes(const vector<Shape> & shapes)
{
    this->etalonShapes = shapes;
}

void ShapeDetector::setParameters(double aproximationAcuracy, double c1, double c2,
        DetectionMethod method, double inaccuracyPercent, int erosion_size)
{
    this->aproximationAcuracy = aproximationAcuracy;
    this->detectionCoef1 = c1;
    this->detectionCoef2 = c2;
    this->detectionMethod = method;
    this->inaccuracyPercent = inaccuracyPercent;
    this->erosion_size = erosion_size;
}

double ShapeDetector::getAproximationAcuracy()
{
    return aproximationAcuracy;
}
















QJsonObject ShapeDetector::shapePropertyToJsonObject(const ShapeProperty & shapeProperty)
{
    QJsonObject shapePropertyJsonObject;

    shapePropertyJsonObject.insert("value", QJsonValue(shapeProperty.value));
    shapePropertyJsonObject.insert("inaccuracy", QJsonValue(shapeProperty.inaccuracy));
    shapePropertyJsonObject.insert("enable", QJsonValue(shapeProperty.enable));

    return shapePropertyJsonObject;
}

QJsonArray ShapeDetector::typesToJsonArray(const map<ShapeType, double> & types)
{
    QJsonArray typesJsonArray;
    map<ShapeType, double>::const_iterator typeIt;
    for ( typeIt = types.begin(); typeIt != types.end(); typeIt++ ) {
        QJsonObject typeJsonObject;
        typeJsonObject.insert("type", QJsonValue(QString::fromStdString(typeIt->first)));
        typeJsonObject.insert("chance", QJsonValue(typeIt->second));
        typesJsonArray.append(typeJsonObject);
    }

    return typesJsonArray;
}

QJsonArray ShapeDetector::sideAndSideAndShapePropertyToJsonArray(const map<pair<int, int>, ShapeProperty> &sidesAndShapeProperties)
{
    QJsonArray sidesAndProperteisJsonArray;
    map<pair<int, int>, ShapeProperty>::const_iterator it;
    for ( it = sidesAndShapeProperties.begin(); it != sidesAndShapeProperties.end(); it++ ) {
        QJsonObject sidesAndPropertyJsonObject;
        QJsonObject propetyJsonObject = shapePropertyToJsonObject(it->second);
        sidesAndPropertyJsonObject.insert("side1", it->first.first);
        sidesAndPropertyJsonObject.insert("side2", it->first.second);
        sidesAndPropertyJsonObject.insert("property", propetyJsonObject);
        sidesAndProperteisJsonArray.append(sidesAndPropertyJsonObject);
    }

    return sidesAndProperteisJsonArray;
}

QJsonArray contourToJsonArray(const ShapeContour & shapeContour) {
    QJsonArray contourJsonArray;
    ShapeContour::const_iterator pointIt;
    for ( pointIt = shapeContour.begin(); pointIt != shapeContour.end(); pointIt++ ) {
        QJsonObject pointJsonObject;

        pointJsonObject.insert("x",pointIt->x);
        pointJsonObject.insert("y",pointIt->y);

        contourJsonArray.append(pointJsonObject);
    }

    return contourJsonArray;
}

QJsonObject ShapeDetector::shapeToJsonObject(const ShapeWithContour& shape, bool withContours, bool withAnglesAndSidesRatios)
{
    QJsonObject shapeJsonObject;

    // type
    QJsonArray typesJsonArray = typesToJsonArray(shape.getTypes());
    shapeJsonObject.insert("types", typesJsonArray);

    // corners
    shapeJsonObject.insert("cornersCnt", QJsonValue((double)shape.getProperties().getCornersCount()));
    shapeJsonObject.insert("cornersCntEnable", QJsonValue(shape.getProperties().getCornersCountEnable()));

    // moment
    QJsonObject momentJsonObject = shapePropertyToJsonObject(shape.getProperties().getMoment());
    shapeJsonObject.insert("moment", momentJsonObject);

    // angles
    QJsonArray anglesJsonArray;
    if ( withAnglesAndSidesRatios ) {
        anglesJsonArray = sideAndSideAndShapePropertyToJsonArray(shape.getProperties().getAngles());
    }
    shapeJsonObject.insert("angles", anglesJsonArray);

    // sidesRatios
    QJsonArray sidesRatiosJsonArray;
    if ( withAnglesAndSidesRatios ) {
        sidesRatiosJsonArray = sideAndSideAndShapePropertyToJsonArray(shape.getProperties().getSidesRatios());
    }
    shapeJsonObject.insert("sidesRatios", sidesRatiosJsonArray);

    // contour
    QJsonArray contourPointsJsonArray;
    if ( withContours ) {
        contourPointsJsonArray = contourToJsonArray(shape.getContour());
    }
    shapeJsonObject.insert("contour", contourPointsJsonArray);

    return shapeJsonObject;
}

QJsonArray ShapeDetector::shapesToJsonArray(const vector<ShapeWithContour> &shapes, bool withContours, bool withAnglesAndSidesRatios)
{
    QJsonArray shapesJsonArray;

    vector<ShapeWithContour>::const_iterator shapesIt;
    for(shapesIt = shapes.begin(); shapesIt != shapes.end(); shapesIt++) {
        QJsonObject shapeJsonObject = ShapeDetector::shapeToJsonObject(*shapesIt, withContours, withAnglesAndSidesRatios);
        shapesJsonArray.append(shapeJsonObject);
    }

    return shapesJsonArray;
}

QByteArray ShapeDetector::saveShapesToJson(const vector<ShapeWithContour> &shapes, bool withContours, bool withAnglesAndSidesRatios)
{ 
    QJsonArray shapesJsonArray = ShapeDetector::shapesToJsonArray(shapes, withContours, withAnglesAndSidesRatios);
    QJsonDocument shapesJsonDocument;
    shapesJsonDocument.setArray(shapesJsonArray);
    QByteArray shapesJsonContent = shapesJsonDocument.toJson();
    return shapesJsonContent;
}

ShapeProperty ShapeDetector::jsonObjectToShapeProperty(const QJsonObject &jsonObject)
{
    double valuel = jsonObject.value("value").toDouble();
    double inacuracy = jsonObject.value("inaccuracy").toDouble();
    bool enable = jsonObject.value("enable").toBool();
    return ShapeProperty(valuel, inacuracy, enable);
}

map<pair<int, int>, ShapeProperty> ShapeDetector::jsonArrayToSideAndSideAndShapeProperty(const QJsonArray &jsonArray)
{
    map<pair<int, int>, ShapeProperty> result;
    QJsonArray::const_iterator jsonIt;
    for(jsonIt = jsonArray.begin(); jsonIt != jsonArray.end(); jsonIt++) {
        QJsonObject jsonObject = (*jsonIt).toObject();
        int side1 = boost::math::round(jsonObject.value("side1").toDouble());
        int side2 = boost::math::round(jsonObject.value("side2").toDouble());
        QJsonObject shapePropertyJsonObject = jsonObject.value("property").toObject();
        ShapeProperty shapeProperty = ShapeDetector::jsonObjectToShapeProperty(shapePropertyJsonObject);
        result[pair<int, int>(side1, side2)] = shapeProperty;
    }

    return result;
}

map<ShapeType, double> ShapeDetector::jsonArrayToShapeTypes(const QJsonArray &jsonArray)
{
    std::map<ShapeType, double> types;
    QJsonArray::const_iterator jsonIt;
    for(jsonIt = jsonArray.begin(); jsonIt != jsonArray.end(); jsonIt++) {
        QJsonObject jsonObject = (*jsonIt).toObject();
        ShapeType type = jsonObject.value("type").toString().toStdString();
        double chance = jsonObject.value("chance").toDouble();
        types[type] = chance;
    }

    return types;
}

Shape ShapeDetector::jsonObjectToShape(const QJsonObject &jsonObject)
{
    // type
    map<ShapeType, double> types = ShapeDetector::jsonArrayToShapeTypes(jsonObject.value("types").toArray());

    // corners
    int shapeCornersCnt = boost::math::round(jsonObject.value("cornersCnt").toDouble());
    bool shapeCornersCntEnable = jsonObject.value("cornersCntEnable").toBool();

    // moment
    QJsonObject momentJsonObject = jsonObject.value("moment").toObject();
    ShapeProperty moment = ShapeDetector::jsonObjectToShapeProperty(momentJsonObject);

    // angles
    QJsonArray anglesJsonArray = jsonObject.value("angles").toArray();
    map<pair<int, int>, ShapeProperty> angles = jsonArrayToSideAndSideAndShapeProperty(anglesJsonArray);

    // sidesRatios
    QJsonArray sidesRatiosJsonArray = jsonObject.value("sidesRatios").toArray();
    map<pair<int, int>, ShapeProperty> sidesRatios = jsonArrayToSideAndSideAndShapeProperty(sidesRatiosJsonArray);

    // shape properties
    ShapeProperties shapeProps = ShapeProperties(shapeCornersCnt);
    shapeProps.setCornersCountEnable(shapeCornersCntEnable);
    shapeProps.setMoment(moment);
    shapeProps.setAngles(angles);
    shapeProps.setSidesRatios(sidesRatios);

    return Shape(shapeProps, types.begin()->first);                     // TODO2: load all types with their chances (now its load only first with defaukt chance = 1)
}

vector<Shape> ShapeDetector::loadShapesFromJson(const QByteArray & shapesJsonContent)
{
    vector<Shape> shapes;

    QJsonDocument shapesJsonDocument = QJsonDocument::fromJson(shapesJsonContent);
    QJsonArray shapesJsonArray = shapesJsonDocument.array();
    QJsonArray::Iterator shapejsonIt;
    for(shapejsonIt = shapesJsonArray.begin(); shapejsonIt != shapesJsonArray.end(); shapejsonIt++) {
        QJsonObject shapeJsonObject = (*shapejsonIt).toObject();
        shapes.push_back(ShapeDetector::jsonObjectToShape(shapeJsonObject));
    }

    return shapes;
}

/*
    // TODO: it is just for tests here
    ShapeProperties propsEllipse1 = ShapeProperties(0);
    propsEllipse1.setCornersCountEnable(false);
    propsEllipse1.setMoment(ShapeProperty(1, 0.2, true));
    shapes.push_back(Shape(propsEllipse1, "propsEllipse1"));

    ShapeProperties propsEllipse2 = ShapeProperties(0);
    propsEllipse2.setCornersCountEnable(false);
    propsEllipse2.setMoment(ShapeProperty(0.5, 0.2, true));
    shapes.push_back(Shape(propsEllipse2, "propsEllipse2"));

    ShapeProperties propsTriangle1 = ShapeProperties(3);
    shapes.push_back(Shape(propsTriangle1, "propsTriangle1"));

    ShapeProperties propsRectangle1 = ShapeProperties(4);
    shapes.push_back(Shape(propsRectangle1, "propsRectangle1"));
*/

