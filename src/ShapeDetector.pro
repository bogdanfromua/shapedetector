#-------------------------------------------------
#
# Project created by QtCreator 2013-11-17T19:14:18
#
#-------------------------------------------------

QT       += core gui

QT += widgets

TARGET = ShapeDetector
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    shapedetector.cpp \
    shape.cpp \
    shapeproperties.cpp \
    shapeproperty.cpp \
    shapewithcontour.cpp

HEADERS  += mainwindow.h \
    shapedetector.h \
    shape.h \
    shapeproperties.h \
    shapeproperty.h \
    shapecontour.h \
    shapewithcontour.h

FORMS    += mainwindow.ui

CONFIG += link_pkgconfig

PKGCONFIG += opencv
