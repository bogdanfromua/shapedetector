#include "shape.h"


using namespace std;


Shape::Shape(const ShapeProperties &properties)
    :properties(properties) {}

Shape::Shape(const ShapeProperties &properties, const map<ShapeType, double> &types)
    :properties(properties), types(types) {}

Shape::Shape(const ShapeProperties &properties, const ShapeType &type)
    :properties(properties)
{
    types[type] = 1;
}

std::map<ShapeType, double> Shape::getTypes() const
{
    return types;
}

ShapeProperties Shape::getProperties() const
{
    return properties;
}

void Shape::setTypes(std::map<ShapeType, double> types)
{
    this->types = types;
}

void Shape::setProperties(ShapeProperties properties)
{
    this->properties = properties;
}

pair<ShapeType, double> Shape::getBestType() const
{
    pair<ShapeType, double> bestTyepe(UNKNOWN, 0);

    map<ShapeType, double>::const_iterator it;
    for (it = types.begin(); it != types.end(); it++) {
        if (it->second > bestTyepe.second) {
            bestTyepe = *it;
        }
    }

    return bestTyepe;
}
