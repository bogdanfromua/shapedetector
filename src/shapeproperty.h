#ifndef SHAPEPROPERTY_H
#define SHAPEPROPERTY_H


class ShapeProperty
{
public: // fields
    double value;
    double inaccuracy;
    bool enable;

public: // methods
    ShapeProperty(){}
    ShapeProperty(double value, double inaccuracy, bool enable )
        :value(value), inaccuracy(inaccuracy), enable(enable) {}
    double isSubsetOf(const ShapeProperty &superSetShapeProperty) const;
};


#endif // SHAPEPROPERTY_H
