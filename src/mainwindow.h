#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "shapedetector.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
public slots:
    void loadEtalonShapesFromFile();
    void saveEtalonShapesToFile();
    void saveResultShapesToFile();
    void detectShapes(bool withClasification);

private slots:
    void on_saveEtalonShapesButton_clicked();
    void on_loadEtalonShapesButton_clicked();
    void on_detectAndClassifyShapesButton_clicked();
    void on_saveResultShapesButton_clicked();

    void on_setImagePathButton_clicked();

private:
    Ui::MainWindow *ui;
    ShapeDetector shapeDetector;
    //Image srcImage;
    //QString etalonShapes;
    //QString resultShapes;
};

#endif // MAINWINDOW_H
