#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "shapedetector.h"
#include <string>
#include <iostream>
#include <stdexcept>
#include "boost/function.hpp"
#include "boost/bind.hpp"
#include <functional>
#include "/usr/local/include/opencv2/highgui/highgui.hpp"
#include <QByteArray>
#include <QFile>
#include <QFileDialog>


using namespace std;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::loadEtalonShapesFromFile()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Open etalon shapes"), "", tr("Files (*.*)"));
    QFile shapesJsonFile(filePath);
    shapesJsonFile.open(QIODevice::ReadOnly);
    QByteArray shapesJsonContent = shapesJsonFile.readAll();
    shapesJsonFile.close();
    ui->etalonShapesEdit->setPlainText(QString(shapesJsonContent));
}

void MainWindow::saveEtalonShapesToFile()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save etalon shapes"), "", tr("Files (*.*)"));
    string etalonShapesJsonStdStr = ui->etalonShapesEdit->toPlainText().toStdString();
    QByteArray shapesJsonContent((const char*) etalonShapesJsonStdStr.c_str(), etalonShapesJsonStdStr.size() * 1);
    QFile shapesJsonFile(filePath);
    shapesJsonFile.open(QIODevice::ReadWrite);
    shapesJsonFile.write(shapesJsonContent);
    shapesJsonFile.close();
}

void MainWindow::saveResultShapesToFile()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save result shapes"), "", tr("Files (*.*)"));
    string resultShapesJsonStdStr = ui->resultShapesEdit->toPlainText().toStdString();
    QByteArray shapesJsonContent((const char*) resultShapesJsonStdStr.c_str(), resultShapesJsonStdStr.size() * 1);
    QFile shapesJsonFile(filePath);
    shapesJsonFile.open(QIODevice::ReadWrite);
    shapesJsonFile.write(shapesJsonContent);
    shapesJsonFile.close();
}

void setShapeEmptyType(Shape & shape)
{
    std::map<ShapeType, double> types;
    types[UNKNOWN] = 1;
    shape.setTypes(types);
}

void removeSameSidesRatios(Shape & shape)
{
    ShapeProperties newProperties = shape.getProperties();
    newProperties.removeSameSides();
    shape.setProperties(newProperties);
}

void MainWindow::detectShapes(bool withClasification)
{
    try {
        // set settings
        DetectionMethod method;
        if ( ui->mathodBox->currentIndex() == 0 ) {
            method = CANNY_EXTERNAL;
        } else if ( ui->mathodBox->currentIndex() == 1 ) {
            method = THRESHOLD_EXTERNAL;
        } else if ( ui->mathodBox->currentIndex() == 2 ) {
            method = CANNY_TREE;
        } else if ( ui->mathodBox->currentIndex() == 3 ) {
            method = THRESHOLD_TREE;
        }

        shapeDetector.setParameters(
                    ui->acuracyEdit->text().toDouble(),
                    ui->c1Edit->text().toDouble(),
                    ui->c2Edit->text().toDouble(),
                    method,
                    ui->inacuracyPcEdit->text().toDouble(),
                    (int)ui->erosionSizeEdit->text().toDouble()
        );

        if ( withClasification == true ) {
            // set etalon sapes
            string etalonShapesJsonStdStr = ui->etalonShapesEdit->toPlainText().toStdString();
            QByteArray shapesJsonContent((const char*) etalonShapesJsonStdStr.c_str(), etalonShapesJsonStdStr.size() * 1);
            vector<Shape> shapes = ShapeDetector::loadShapesFromJson(shapesJsonContent);
            shapeDetector.setEtalonShapes(shapes);
        }

        // load image
        //QString srcImagePath = QFileDialog::getOpenFileName(this, tr("Open image"), "", tr("Files (*.*)"));
        Image srcImage = ShapeDetector::readImage(ui->imagePathEdit->text().toStdString());
        ShapeDetector::outImage(srcImage, "source");

        // detect
        vector<ShapeWithContour> classifiedShapes = shapeDetector.getShapesFromImage(srcImage);

        if ( withClasification == true ) {
            // classify
            boost::function<void (Shape&)> f(boost::bind(&ShapeDetector::classifyShape, &shapeDetector, _1));
            for_each(classifiedShapes.begin(), classifiedShapes.end(), f);
        } else {
            // add unknown type
            for_each(classifiedShapes.begin(), classifiedShapes.end(), setShapeEmptyType);
        }

        // removeSameSidesRatios
        if ( ui->removeSameSidesBox->isChecked() == true ) {
            for_each(classifiedShapes.begin(), classifiedShapes.end(), removeSameSidesRatios);
        }

        // out result image
        Image resultImage = ShapeDetector::markShapesOnImage(srcImage, classifiedShapes);
        ShapeDetector::outImage(resultImage, "result");

        // out result json
        QByteArray classifiedShapesContent = ShapeDetector::saveShapesToJson(classifiedShapes, ui->saveContoursBox->isChecked(), ui->saveAnglesAndRatiosBox->isChecked());
        ui->resultShapesEdit->setPlainText(QString(classifiedShapesContent));
    }
    catch (exception e) {
    }
}

void MainWindow::on_saveEtalonShapesButton_clicked()
{
    saveEtalonShapesToFile();
}

void MainWindow::on_loadEtalonShapesButton_clicked()
{
    loadEtalonShapesFromFile();
}

void MainWindow::on_detectAndClassifyShapesButton_clicked()
{
    detectShapes(ui->classifyBox->isChecked());
}

void MainWindow::on_saveResultShapesButton_clicked()
{
    saveResultShapesToFile();
}

void MainWindow::on_setImagePathButton_clicked()
{
    ui->imagePathEdit->setText(QFileDialog::getOpenFileName(this, tr("Open image"), "", tr("Files (*.*)")));
}
