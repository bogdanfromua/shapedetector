#include "shapewithcontour.h"


using namespace std;


ShapeWithContour::ShapeWithContour(const ShapeContour & contour, double inaccuracyPercent)
    :Shape(ShapeProperties(contour, inaccuracyPercent)), contour(contour) {}

ShapeWithContour::ShapeWithContour(const ShapeContour & contour, const ShapeProperties & properties)
    :Shape(properties), contour(contour) {}

ShapeWithContour::ShapeWithContour(const ShapeContour & contour, double inaccuracyPercent, const map<ShapeType, double> & types)
    :Shape(ShapeProperties(contour, inaccuracyPercent), types), contour(contour) {}

ShapeWithContour::ShapeWithContour(const ShapeContour & contour, const ShapeProperties & properties, const map<ShapeType, double> & types)
    :Shape(properties, types), contour(contour) {}

ShapeContour ShapeWithContour::getContour() const
{
    return contour;
}

void ShapeWithContour::setContour(const ShapeContour &contour)
{
    this->contour = contour;
}
